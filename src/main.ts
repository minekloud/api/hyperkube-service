import { ValidationPipe } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { ConfigService } from '@nestjs/config'
import { PodsService } from './kubernetes/pods.service'
import { ExecService } from './kubernetes/exec.service'
// DEBUG / DEV PURPOSE
//import { ServersController } from './servers/servers.controller';

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.useGlobalPipes(new ValidationPipe({ transform: true }))

  const config: ConfigService = app.get(ConfigService)
  app.setGlobalPrefix(config.get<string>('GLOBAL_PREFIX') || '/api/hyperkube')

  await app.listen(config.get<number>('APP_PORT'))

  
  // DEBUG / DEV PURPOSE
  /*
  const service = await app.resolve(ExecService);
  try {
    await service.execCommandOnServer('default', '24', "gamemode set Lif 1");
  } catch (e) {
    console.log(e);
  }
  */
  
}
bootstrap()
