export enum ServerStatus {
  ON = 'on',
  OFF = 'off',
  LOADING = 'loading',
}
