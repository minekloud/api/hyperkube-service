import { HttpModule, Module } from '@nestjs/common'
import { KubernetesModule } from 'src/kubernetes/kubernetes.module'
import { Reconciliator } from './reconciliator.service'
import { ServersController } from './servers.controller'
import { ServersService } from './servers.service'

@Module({
  controllers: [ServersController],
  providers: [ServersService, Reconciliator],
  imports: [KubernetesModule, HttpModule],
})
export class ServersModule {}
