import { CoreV1Api, Exec } from '@kubernetes/client-node'
import { Injectable } from '@nestjs/common'
import { Readable, Writable } from 'stream'
import { ConfigService } from './config.service'
import { PodsService } from './pods.service'

@Injectable()
export class ExecService {
  private exec: Exec

  constructor(
    private configService: ConfigService,
    private podsServide: PodsService,
  ) {
    this.exec = new Exec(configService.getKubeConfig())
  }

  private async execCommand(namespace: string, podName: string, containerName: string, command: string[]) {
    
    await this.exec.exec(
      namespace,
      podName,
      containerName,
      command,
      process.stdout as Writable,
      process.stderr as Writable,
      process.stdin as Readable,
      false,
      (status) => {
        console.log(status.status);
        if(status.details) {
          console.log(status.details)
        }
      },
    )
  }

  async execCommandOnServer(
    namespace: string,
    serverId: string,
    command: string,
  ): Promise<boolean> {
    console.log(`[${namespace} ${serverId}] Executing command "${command}"`);

    const pod = await this.podsServide.getServerPod(namespace, serverId)
    if (!pod) {
      return false
    }

    const safeCommand = command.replace(`'`, `'"'"'`)

    await this.execCommand(
      namespace,
      pod.metadata.name,
      pod.spec.containers[0].name,
      ['sh', '-c', `echo '${safeCommand}' > /proc/\`pidof java\`/fd/0`],
    )

    return true
  }
}
