import { Test, TestingModule } from '@nestjs/testing'
import { PersistentVolumeClaimsService } from './persistent-volume-claims.service'

describe('PersistentVolumeClaimsService', () => {
  let service: PersistentVolumeClaimsService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PersistentVolumeClaimsService],
    }).compile()

    service = module.get<PersistentVolumeClaimsService>(
      PersistentVolumeClaimsService,
    )
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })
})
