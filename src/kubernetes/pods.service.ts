import { AppsV1Api, CoreV1Api, V1Pod } from '@kubernetes/client-node'
import { Injectable } from '@nestjs/common'
import { ConfigService } from './config.service'

@Injectable()
export class PodsService {
  client: CoreV1Api

  constructor(configService: ConfigService) {
    this.client = configService.getCoreClient()
  }

  async getServerPod(namespace: string, serverId: string): Promise<V1Pod | false> {
    const res = await this.client.listNamespacedPod(
      namespace,
      undefined,
      undefined,
      undefined,
      undefined,
      `serverId=${serverId}`,
    )

    if(res.body.items.length < 1) {
        console.error(`No pod found for server ${serverId}`);
        return false;
    }

    if(res.body.items.length > 1) {
        // This should not happend but who know
        console.warn(`Warning more than one pod found for server ${serverId}, will use the first one`);
    }

    return res.body.items[0];
  }
}
